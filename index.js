const path = require('path')

module.exports = {
  name: 'taredm_blog',
  plugins: [
    '@vuepress/blog',
    '@vuepress/google-analytics',
    '@vuepress/pagination',
    [
      '@vuepress/search', {
      searchMaxSuggestions: 10
    }],
    [
      '@vuepress/register-components', {
      componentsDir: [
        path.resolve(__dirname, 'components')
      ]
    }]
  ]
}