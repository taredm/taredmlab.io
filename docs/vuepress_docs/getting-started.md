# Getting Started

설치하기 전에 node 버전 확인

## Installation

```
yarn global add vuepress
```

## Project Setup

```
# install as a local dependency
yarn add -D vuepress # OR npm install -D vuepress

# create a docs directory
mkdir docs
# create a markdown file
echo '# Hello VuePress' > docs/README.md
```

```package.json```에 다음과 같이 추가
```
{
  "scripts": {
    "docs:dev": "vuepress dev docs",
    "docs:build": "vuepress build docs"
  }
}
```

실행
```
yarn docs:dev # OR npm run docs:dev
```

빌드
```
yarn docs:build # OR npm run docs:build
```