# Introduction

VuePress는 SSG(Static Site Generator)중 하나로 Vue를 사용하여 만들어 졌습니다. SSG는 Jekyll, Hexo, GitBooks등이 있으며 VuePress는 자료가 많지 않은 것으로 보아 아직 인기는 없는 듯 합니다. 일단 VuePress를 이용하여 GitLab pages를 만들어 봅니다.

## VuePress

[VuePress](https://vuepress.vuejs.org/)에서 자세한 내용을 확인할 수 있습니다.

## GitLab Pages

간단한 웹사이트를 만들 수 있습니다. 자료 정리용을 적당할 것 같습니다.