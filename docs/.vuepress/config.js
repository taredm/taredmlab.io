const dirTree = require('directory-tree');
const path = require('path');
const groups = [
  {
    group_dir: "blog",
    group_title: "Blog",
  },
  {
    group_dir: "vuepress_docs",
    group_title: "VuePress",
  }
]

module.exports = {
    title: "taredm's website",
    description: "gitlab pages, powered by vuepress",
    dest: "public",
    ga: "UA-131638911-1",
    themeConfig: {
      sidebar: getSidebar(groups),
      nav: [
        { text: 'Home', link: '/' },
        { text: 'Blog', link: '/blog/' },
        { text: 'GitLab', link: 'https://gitlab.com/taredm'}
        ]
    },
    markdown: {
      // options for markdown-it-anchor
      //anchor: { permalink: false },
      // options for markdown-it-toc
      //toc: { includeLevel: [1, 2] },
      extendMarkdown: md => {
        // use more markdown-it plugins!
        md.use(require('markdown-it')),
        md.use(require('markdown-it-mermaid').default),
        md.use(require('markdown-it-katex'))
      }
    }
   }

function getSidebar (group_list) {
  var arr = []

  for (i in group_list) {
    group_dir = group_list[i].group_dir;
    group_title = group_list[i].group_title;

    var files = dirTree(path.join(__dirname, '../', group_dir), {extensions:/\.md/});
    var posts = files.children.map(children => path.parse(children.name).name !== 'README' ? ('/'+group_dir+'/'+path.parse(children.name).name) : ('/' + group_dir + '/') );
    var idx = posts.indexOf('/' + group_dir + '/');
    posts.splice(idx, 1);
    posts.splice(0,0,'/' + group_dir + '/');
    var group_object = {
      title: group_title,
      children: posts,
    };

    arr.push(group_object);
  }
  
  return arr
}